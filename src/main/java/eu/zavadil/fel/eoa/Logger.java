package eu.zavadil.fel.eoa;

public class Logger {

    public static void out(String o, Object... args) {
        System.out.println(String.format(o, args));
    }

    public static void out(Exception ex) {
        out("Exception %s: %s", ex.getClass().getName(), ex.getMessage());
    }

}