package eu.zavadil.fel.eoa.core;

import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Implementuje obecnou evolucni strategii
 * Hleda hodnotu s nejvyssi kvalitou, proto, pokud je treba minimalizovat nejakou funkci, invertujte hodnotu ve fitness funkci
 *
 * @param <Genome> Prostor, ve kterem vyhledavame.
 * @param <Fitness> Skalar, ve kterem reprezentujeme kvalitu.
 */
public class EvolutionStrategyWithMutation<Genome, Fitness extends Comparable<Fitness>> extends EvolutionStrategy<Genome, Fitness> {

    public EvolutionStrategyWithMutation(Function<Integer, Population<Genome>> initializeFunction, Function<Genome, Fitness> fitnessFunction, int minimumPopulationSize, int offspringCount, Function<Genome, Genome> mutationFunction, Predicate<Population<Genome>> terminationCondition) {
        super(mutationFunction, initializeFunction, fitnessFunction, minimumPopulationSize, offspringCount, terminationCondition);
    }

    @Override
    protected Population<Genome> breed(Population<Genome> population) {
        Population<Genome> newPopulation = population.clone();
        for (Genome parent: population) {
            for (int i = 0; i < this.offspringCount; i++) {
                newPopulation.add(this.mutate(parent));
            }
        }
        return newPopulation;
    }

    @Override
    protected int getIterations() {
        return this.mutations;
    }

}
