package eu.zavadil.fel.eoa.core;

import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;

public class EvolutionStrategyWithCrossoverAndMutation<Genome, Fitness extends Comparable<Fitness>> extends EvolutionStrategyWithCrossover<Genome, Fitness> {

    private final Double mutationProbability;

    public EvolutionStrategyWithCrossoverAndMutation(Function<Integer, Population<Genome>> initializeFunction, Function<Genome, Fitness> fitnessFunction, int minimumPopulationSize, int offspringCount, BiFunction<Genome, Genome, Genome> crossoverFunction, Double mutationProbability, Function<Genome, Genome> mutationFunction, Predicate<Population<Genome>> terminationCondition) {
        super(initializeFunction, mutationFunction, fitnessFunction, minimumPopulationSize, offspringCount, crossoverFunction, terminationCondition);
        this.mutationProbability = mutationProbability;
    }

    /**
     * vrati true s pravdepodobnosti mutationProbability
     * @return
     */
    private boolean shouldMutate() {
        return this.getRandomGenerator().nextDouble() < this.mutationProbability;
    }

    @Override
    protected Population<Genome> breed(Population<Genome> population) {
        Population<Genome> parents = population.clone();
        Population<Genome> offsprings = population.clone();
        while (parents.size() > 1) {
            Genome father = parents.remove(this.getRandom(parents.size()));
            Genome mother = parents.remove(this.getRandom(parents.size()));
            Population<Genome> children = this.mate(father, mother);
            for (Genome child: children) {
                if (this.shouldMutate()) {
                    offsprings.add(this.mutate(child));
                } else {
                    offsprings.add(child);
                }
            }
        }
        return offsprings;
    }
}
