package eu.zavadil.fel.eoa.core;

import java.util.LinkedList;

public class Population<Genome> extends LinkedList<Genome> {

    @Override
    public Population<Genome> clone() {
        return (Population<Genome>) super.clone();
    }

    public void add(Population<Genome> anotherPopulation) {
        this.addAll(anotherPopulation);
    }

}
