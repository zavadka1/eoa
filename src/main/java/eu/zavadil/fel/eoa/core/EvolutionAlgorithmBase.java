package eu.zavadil.fel.eoa.core;

import java.util.Random;
import java.util.function.Function;

/**
 * Obecny evolucni algoritmus pro prostor (Genome) a kvalitu (Fitness).
 *
 * @param <Genome> Prostor, ve kterem vyhledavame.
 * @param <Fitness> Skalar, ve kterem reprezentujeme kvalitu.
 */
public class EvolutionAlgorithmBase<Genome, Fitness extends Comparable<Fitness>> {

    protected int mutations = 0;

    protected final Function<Genome, Genome> mutationFunction;

    protected final Function<Genome, Fitness> fitnessFunction;

    public EvolutionAlgorithmBase(Function<Genome, Genome> mutationFunction, Function<Genome, Fitness> fitnessFunction) {
        this.mutationFunction = mutationFunction;
        this.fitnessFunction = fitnessFunction;
    }

    private Random randomGenerator;

    protected Random getRandomGenerator() {
        if (this.randomGenerator == null) {
            this.randomGenerator = new Random();
        }
        return this.randomGenerator;
    }

    /**
     * vrati nahodny integer mezi 0 (vcetne) a max (nejvyssi mozna hodnota je max - 1)
     * @param max
     * @return
     */
    protected int getRandom(int max) {
        return this.getRandomGenerator().nextInt(max);
    }

    protected Genome mutate(Genome individual) {
        this.mutations++;
        return this.mutationFunction.apply(individual);
    }

    protected Fitness calculateFitness(Genome individual) {
        return this.fitnessFunction.apply(individual);
    }

}
