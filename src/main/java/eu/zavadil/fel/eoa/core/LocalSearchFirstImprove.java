package eu.zavadil.fel.eoa.core;

import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * Implementuje lokalni prohledavani pro obecny prostor (Genome) a kvalitu (Fitness).
 * Hleda hodnotu s nejvyssi kvalitou, proto, pokud je treba nejakou funkci minimalizovat, invertujte hodnotu ve fitness funkci (f = 1/v nebo f = -v)
 *
 * @param <Genome> Prostor, ve kterem vyhledavame.
 * @param <Fitness> Skalar, ve kterem reprezentujeme kvalitu.
 */
public class LocalSearchFirstImprove<Genome, Fitness extends Comparable<Fitness>> extends EvolutionAlgorithmBase<Genome, Fitness> {

    private final Supplier<Genome> initializeFunction;

    private final Predicate<Genome> terminationCondition;

    public LocalSearchFirstImprove(Supplier<Genome> initializeFunction, Function<Genome, Genome> mutateFunction, Function<Genome, Fitness> fitnessFunction, Predicate<Genome> terminationCondition) {
        super(mutateFunction, fitnessFunction);
        this.initializeFunction = initializeFunction;
        this.terminationCondition = terminationCondition;
    }

    private boolean shouldTerminate(Genome individual) {
        return this.terminationCondition.test(individual);
    }

    private Genome initialize() {
        return this.initializeFunction.get();
    }

    public EvolutionResult<Genome, Fitness> run(String name, int maxIterations) {
        EvolutionResult<Genome, Fitness> result = new EvolutionResult<>(name, maxIterations, this.fitnessFunction);
        Genome x = this.initialize();
        Integer i = 0;
        result.addToHistory(i, x);
        while (i < maxIterations && !this.shouldTerminate(x)) {
            Genome y = this.mutate(x);
            if (this.calculateFitness(y).compareTo(this.calculateFitness(x)) > 0) {
                x = y;
                result.addToHistory(i, x);
            }
            i++;
        }
        return result;
    }

}
