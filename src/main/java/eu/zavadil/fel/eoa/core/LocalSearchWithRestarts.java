package eu.zavadil.fel.eoa.core;

import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class LocalSearchWithRestarts<Genome, Fitness extends Comparable<Fitness>> extends EvolutionAlgorithmBase<Genome, Fitness> {

    private final int restarts;

    private final Supplier<Genome> initializeFunction;

    private final Predicate<Genome> terminationCondition;

    public LocalSearchWithRestarts(Supplier<Genome> initializeFunction, Function<Genome, Genome> mutateFunction, Function<Genome, Fitness> fitnessFunction, Predicate<Genome> terminationCondition, int restarts) {
        super(mutateFunction, fitnessFunction);
        this.initializeFunction = initializeFunction;
        this.terminationCondition = terminationCondition;
        this.restarts = restarts;
    }

    public EvolutionResult<Genome, Fitness> run(String name, Integer maxIterations) {
        final int iterationsPerRun = Math.floorDiv(maxIterations, this.restarts);
        EvolutionResult<Genome, Fitness> result = new EvolutionResult<>(name, maxIterations, this.fitnessFunction);
        for (int i = 0; i < this.restarts; i++) {
            LocalSearchFirstImprove<Genome, Fitness> ls = new LocalSearchFirstImprove<>(
                this.initializeFunction,
                this.mutationFunction,
                this.fitnessFunction,
                this.terminationCondition
            );
            EvolutionResult<Genome, Fitness> resultLS = ls.run(name, iterationsPerRun);
            if (i == 0) {
                result.merge(resultLS);
            } else {
                if (resultLS.getBestFitness().compareTo(result.getBestFitness()) > 0) {
                    result.addToHistory((i + 1) * iterationsPerRun, resultLS.getBestIndividual());
                }
            }
        }
        return result;
    }

}
