package eu.zavadil.fel.eoa.core;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class EvolutionComparison<Genome, Fitness extends Double> {

    private final Map<String, List<EvolutionResult>> results = new HashMap<>();

    public void addResult(String type, EvolutionResult result) {
        if (!this.results.containsKey(type)) {
            this.results.put(type, new LinkedList<>());
        }
        this.results.get(type).add(result);
    }

    public void exportToCSV(String path) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(path));
        writer.write(this.getSummary());
        writer.write(";\n;\n");
        for (String typeName: this.results.keySet()) {
            writer.write(typeName);
            writer.write(";\n;\n");
            for (EvolutionResult<Genome, Fitness> typeResults: this.results.get(typeName)) {
                writer.write(typeResults.getName());
                writer.write(";\n;\n");
                if (typeResults.getHistory().size() > 0) {
                    for (EvolutionHistory<Fitness> history : typeResults.getHistory()) {
                        writer.write(String.format("%d", history.getIteration()));
                        writer.write(";");
                        writer.write(history.getFitness().toString());
                        writer.write(";\n");
                    }
                    writer.write(String.format("%d", typeResults.getTotalIterations()));
                    writer.write(";");
                    writer.write(typeResults.getBestFitness().toString());
                    writer.write(";\n;\n");
                }
            }
        }
        writer.close();
    }

    public String getSummary() {
        StringBuilder sb = new StringBuilder();
        for (String typeName: this.results.keySet()) {
            sb.append(typeName);
            sb.append(";");
            Double typeSum = 0.0;
            int typeCount = 0;
            for (EvolutionResult<Genome, Fitness> typeResults: this.results.get(typeName)) {
                typeSum += typeResults.getBestFitness().doubleValue();
                typeCount++;
            }
            Double avg = typeSum/typeCount;
            sb.append(avg.toString());
            sb.append(";\n");
        }
        return sb.toString();
    }

}
