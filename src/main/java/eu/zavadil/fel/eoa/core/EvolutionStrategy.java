package eu.zavadil.fel.eoa.core;

import java.util.Comparator;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Implementuje obecnou evolucni strategii - pouziva populaci
 * Hleda hodnotu s nejvyssi kvalitou, proto, pokud je treba minimalizovat nejakou funkci, invertujte hodnotu ve fitness funkci
 *
 * @param <Genome> Prostor, ve kterem vyhledavame.
 * @param <Fitness> Skalar, ve kterem reprezentujeme kvalitu.
 */
public abstract class EvolutionStrategy<Genome, Fitness extends Comparable<Fitness>> extends EvolutionAlgorithmBase<Genome, Fitness> {

    protected final Function<Integer, Population<Genome>> initializeFunction;

    /**
     * Pouze tolik jedincu prezije do dalsi generace.
     */
    protected final int minimumPopulationSize;

    /**
     * Kolik potomku ma jeden par
     */
    protected final int offspringCount;

    protected final Predicate<Population<Genome>> terminationCondition;

    public EvolutionStrategy(Function<Genome, Genome> mutationFunction, Function<Integer, Population<Genome>> initializeFunction, Function<Genome, Fitness> fitnessFunction, int minimumPopulationSize, int offspringCount, Predicate<Population<Genome>> terminationCondition) {
        super(mutationFunction, fitnessFunction);
        this.initializeFunction = initializeFunction;
        this.minimumPopulationSize = minimumPopulationSize;
        this.offspringCount = offspringCount;
        this.terminationCondition = terminationCondition;
    }

    public EvolutionStrategy(Function<Integer, Population<Genome>> initializeFunction, Function<Genome, Fitness> fitnessFunction, int minimumPopulationSize, int offspringCount, Predicate<Population<Genome>> terminationCondition) {
        super(null, fitnessFunction);
        this.initializeFunction = initializeFunction;
        this.minimumPopulationSize = minimumPopulationSize;
        this.offspringCount = offspringCount;
        this.terminationCondition = terminationCondition;
    }

    private boolean shouldTerminate(Population<Genome> population) {
        return this.terminationCondition.test(population);
    }

    protected void sortPopulation(Population<Genome> population) {
        population.sort(Comparator.comparing(this.fitnessFunction::apply));
    }

    protected Population<Genome> initialize() {
        return this.initializeFunction.apply(this.minimumPopulationSize);
    }

    abstract Population<Genome> breed(Population<Genome> population);

    protected Population<Genome> extinct(Population<Genome> population, int populationSize) {
        while (population.size() > populationSize) {population.removeFirst();}
        return population;
    }

    protected abstract int getIterations();

    public EvolutionResult<Genome, Fitness> run(String name, int maxIterations) {
        EvolutionResult<Genome, Fitness> result = new EvolutionResult<>(name, maxIterations, this.fitnessFunction);
        Population<Genome> population = this.initialize();
        Fitness bestFitness = null;
        while (this.getIterations() < maxIterations && !this.shouldTerminate(population)) {
            population = this.extinct(population, this.minimumPopulationSize);
            population = this.breed(population);
            sortPopulation(population);
            Genome bestIndividualOfGeneration = population.getLast();
            Fitness bestFitnessOfGeneration = this.calculateFitness(bestIndividualOfGeneration);
            if (bestFitness == null || bestFitness.compareTo(bestFitnessOfGeneration) < 0) {
                bestFitness = bestFitnessOfGeneration;
                result.addToHistory(this.getIterations(), bestIndividualOfGeneration);
            }
        }
        return result;
    }

}
