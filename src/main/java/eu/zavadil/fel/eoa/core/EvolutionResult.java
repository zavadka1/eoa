package eu.zavadil.fel.eoa.core;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;

public class EvolutionResult<Genome, Fitness> {

    private final String name;

    protected final Function<Genome, Fitness> fitnessFunction;

    private final int totalIterations;

    private int effectiveIterations = 0;

    private Genome bestIndividual = null;

    private final List<EvolutionHistory> history = new LinkedList<>();

    public EvolutionResult(String name, int totalIterations, Function<Genome, Fitness> fitnessFunction) {
        this.name = name;
        this.totalIterations = totalIterations;
        this.fitnessFunction = fitnessFunction;
    }

    private Fitness calculateFitness(Genome individual) {
        return this.fitnessFunction.apply(individual);
    }

    public String getName() {
        return this.name;
    }

    public Genome getBestIndividual() {
        return this.bestIndividual;
    }

    public Fitness getBestFitness() {
        if (this.bestIndividual == null) {
            return null;
        } else {
            return this.calculateFitness(this.bestIndividual);
        }
    }

    public int getTotalIterations() {
        return this.totalIterations;
    }

    /**
     * Po kolika iteracich byl nalezen nejlepsi jedinec.
     * @return
     */
    public int getEffectiveIterations() {
        return this.effectiveIterations;
    }

    public List<EvolutionHistory> getHistory() {
        return this.history;
    }

    public void addToHistory(int iteration, Genome bestIndividual) {
        this.bestIndividual = bestIndividual;
        this.effectiveIterations = iteration;
        this.history.add(new EvolutionHistory(iteration, this.calculateFitness(bestIndividual)));
    }

    public void merge(EvolutionResult<Genome, Fitness> other) {
        for (EvolutionHistory item: other.getHistory()) {
            this.history.add(new EvolutionHistory(item.getIteration(), item.getFitness()));
        }
        this.bestIndividual = other.getBestIndividual();
        this.effectiveIterations = other.getEffectiveIterations();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("Name: %s\n", this.getName()));
        sb.append(String.format("Effective iterations: %d\n", this.getEffectiveIterations()));
        sb.append(String.format("Best fitness: %s\n", this.getBestFitness().toString()));
        sb.append(String.format("Best individual: \n%s\n", this.getBestIndividual().toString()));
        return sb.toString();
    }

}
