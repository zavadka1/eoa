package eu.zavadil.fel.eoa.core;

import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;

public class EvolutionStrategyWithCrossover<Genome, Fitness extends Comparable<Fitness>> extends EvolutionStrategy<Genome, Fitness> {

    protected int crossovers = 0;

    private final BiFunction<Genome, Genome, Genome> crossoverFunction;

    public EvolutionStrategyWithCrossover(Function<Integer, Population<Genome>> initializeFunction, Function<Genome, Fitness> fitnessFunction, int minimumPopulationSize, int offspringCount, BiFunction<Genome, Genome, Genome> crossoverFunction, Predicate<Population<Genome>> terminationCondition) {
        super(initializeFunction, fitnessFunction, minimumPopulationSize, offspringCount, terminationCondition);
        this.crossoverFunction = crossoverFunction;
    }

    public EvolutionStrategyWithCrossover(Function<Integer, Population<Genome>> initializeFunction, Function<Genome, Genome> mutationFunction, Function<Genome, Fitness> fitnessFunction, int minimumPopulationSize, int offspringCount, BiFunction<Genome, Genome, Genome> crossoverFunction, Predicate<Population<Genome>> terminationCondition) {
        super(mutationFunction, initializeFunction, fitnessFunction, minimumPopulationSize, offspringCount, terminationCondition);
        this.crossoverFunction = crossoverFunction;
    }

    protected Genome crossover(Genome male, Genome female) {
        this.crossovers++;
        return this.crossoverFunction.apply(male, female);
    }

    protected Population<Genome> mate(Genome male, Genome female) {
        Population children = new Population<>();
        for (int i = 0; i < this.offspringCount; i++) {
            if (i % 2 == 0) {
                children.add(this.crossover(male, female));
            } else {
                children.add(this.crossover(female, male));
            }
        }
        return children;
    }

    @Override
    protected Population<Genome> breed(Population<Genome> population) {
        Population<Genome> parents = population.clone();
        Population<Genome> offsprings = population.clone();
        while (parents.size() > 1) {
            Genome father = parents.remove(this.getRandom(parents.size()));
            Genome mother = parents.remove(this.getRandom(parents.size()));
            offsprings.add(this.mate(father, mother));
        }
        return offsprings;
    }

    @Override
    protected int getIterations() {
        return this.crossovers;
    }

}
