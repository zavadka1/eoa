package eu.zavadil.fel.eoa.core;

public class EvolutionHistory<Fitness> {

    private final int iteration;

    public int getIteration() {
        return this.iteration;
    }

    private final Fitness fitness;

    public Fitness getFitness() {
        return this.fitness;
    }

    public EvolutionHistory(int iteration, Fitness fitness) {
        this.iteration = iteration;
        this.fitness = fitness;
    }

}
