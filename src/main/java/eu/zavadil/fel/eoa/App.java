package eu.zavadil.fel.eoa;

import eu.zavadil.fel.eoa.core.*;
import eu.zavadil.fel.eoa.trading.graph.*;

import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;

public class App {

    public static void main( String[] args ) {

        /**
         * Název souboru s grafem z TSPLIB
         */
        final String GRAPH_SOURCE_FILE = "bier127.tsp";

        /**
         * Maximalni pocet iteraci na jeden beh algoritmu.
         * U algoritmu s mutaci nebo krizenim se jako jedna iterace pocita jedna mutace resp. jedno krizeni
         */
        final int TOTAL_ITERATIONS = 10000;

        /**
         * Kolikrat spustit kazdy algoritmus
         */
        final int SAMPLES_PER_ALGORITHM = 10;

        // ktere algoritmy spoustet

        final boolean RUN_LOCAL_SEARCH = true;
        final boolean RUN_LOCAL_SEARCH_WITH_RESTARTS = true;
        final boolean RUN_EVOLUTION_STRATEGY_WITH_MUTATION = true;
        final boolean RUN_EVOLUTION_STRATEGY_WITH_CROSSOVER = true;
        final boolean RUN_EVOLUTION_STRATEGY_WITH_MUTATION_AND_CROSSOVER = true;

        /**
         * exportovat historii do CSV?
         */
        final boolean EXPORT_TO_CSV = true;

        /**
         * Nazev CSV souboru, kam budou vysledky exportovany
         */
        final String EXPORT_FILE_PATH = "bier127.CSV";

        /**
         * udela z uplneho grafu neuplny nahodnym smazanim az poloviny hran
         */
        final boolean MAKE_INCOMPLETE = false;

        final EvolutionComparison<TSPSolutionPath, Double> comparison = new EvolutionComparison<>();

        try {

            // ktery graf resit

            //Graph graph = SimpleTestGraph.create();
            //Graph graph = CzechCitiesGraph.create();
            //Graph graph = WorldAirportsGraph.create();

            ClassLoader classloader = Thread.currentThread().getContextClassLoader();
            URL url = classloader.getResource(GRAPH_SOURCE_FILE);
            Path path = Paths.get(url.toURI());
            Graph graph = GraphImporter.importWithCoords(path.toString());

            if (MAKE_INCOMPLETE) {
                graph = IncompleteGraph.makeIncomplete(graph);
            }

            TSPSolutionPath initialSolution = new TSPSolutionPath(graph);

            if (RUN_LOCAL_SEARCH) {
                for (int sampleIndex = 0; sampleIndex < SAMPLES_PER_ALGORITHM; sampleIndex++) {
                    String algorithmName = "Local Search";
                    String sampleName = String.format("Local Search %d", sampleIndex);
                    LocalSearchFirstImprove<TSPSolutionPath, Double> tspLocalSearch = new LocalSearchFirstImprove<>(
                            () -> initialSolution,
                            (v) -> v.mutate(),
                            (v) -> 0.0 - v.cost(),
                            (v) -> false
                    );
                    EvolutionResult<TSPSolutionPath, Double> resultLS = tspLocalSearch.run(sampleName, TOTAL_ITERATIONS);
                    Logger.out(String.format("%s - %s", sampleName, resultLS.getBestFitness().toString()));
                    comparison.addResult(algorithmName, resultLS);
                }
            }

            if (RUN_LOCAL_SEARCH_WITH_RESTARTS) {
                for (int sampleIndex = 0; sampleIndex < SAMPLES_PER_ALGORITHM; sampleIndex++) {
                    String algorithmName = "Local Search with Restarts";
                    String sampleName = String.format("Local Search with Restarts %d", sampleIndex);
                    LocalSearchWithRestarts<TSPSolutionPath, Double> tspLocalSearch = new LocalSearchWithRestarts<>(
                            () -> initialSolution,
                            (v) -> v.mutate(),
                            (v) -> 0.0 - v.cost(),
                            (v) -> false,
                            10
                    );
                    EvolutionResult<TSPSolutionPath, Double> resultLSwR = tspLocalSearch.run(sampleName, TOTAL_ITERATIONS);
                    Logger.out(String.format("%s - %s", sampleName, resultLSwR.getBestFitness().toString()));
                    comparison.addResult(algorithmName, resultLSwR);
                }
            }

            if (RUN_EVOLUTION_STRATEGY_WITH_MUTATION) {
                for (int sampleIndex = 0; sampleIndex < SAMPLES_PER_ALGORITHM; sampleIndex++) {
                    String algorithmName = "Evolution Strategy with Mutation";
                    String sampleName = String.format("Evolution Strategy with Mutation %d", sampleIndex);
                    EvolutionStrategyWithMutation<TSPSolutionPath, Double> tspES = new EvolutionStrategyWithMutation<>(
                            (Integer n) -> {
                                Population<TSPSolutionPath> population = new Population<>();
                                for (int i = 0; i < n; i++) {
                                    population.add(initialSolution.mutate());
                                }
                                return population;
                            },
                            (v) -> 0.0 - v.cost(),
                            10,
                            2,
                            (v) -> v.mutate(),
                            (p) -> false
                    );
                    EvolutionResult<TSPSolutionPath, Double> resultES = tspES.run(sampleName, TOTAL_ITERATIONS);
                    Logger.out(String.format("%s - %s", sampleName, resultES.getBestFitness().toString()));
                    comparison.addResult(algorithmName, resultES);
                }
            }

            if (RUN_EVOLUTION_STRATEGY_WITH_CROSSOVER) {
                for (int sampleIndex = 0; sampleIndex < SAMPLES_PER_ALGORITHM; sampleIndex++) {
                    String algorithmName = "Evolution Strategy with Crossover";
                    String sampleName = String.format("Evolution Strategy with Crossover %d", sampleIndex);
                    EvolutionStrategyWithCrossover<TSPSolutionPath, Double> tspESwC = new EvolutionStrategyWithCrossover<>(
                            (Integer n) -> {
                                Population<TSPSolutionPath> population = new Population<>();
                                for (int i = 0; i < n; i++) {
                                    population.add(initialSolution.mutate());
                                }
                                return population;
                            },
                            (v) -> 0.0 - v.cost(),
                            10,
                            2,
                            (parent1, parent2) -> parent1.crossover(parent2),
                            (p) -> false
                    );
                    EvolutionResult<TSPSolutionPath, Double> resultESwC = tspESwC.run(sampleName, TOTAL_ITERATIONS);
                    Logger.out(String.format("%s - %s", sampleName, resultESwC.getBestFitness().toString()));
                    if (!resultESwC.getBestIndividual().validate()) {
                        Logger.out("Invalid solution!");
                        Logger.out(resultESwC.getBestIndividual().toString());
                    }
                    comparison.addResult(algorithmName, resultESwC);
                }
            }

            if (RUN_EVOLUTION_STRATEGY_WITH_MUTATION_AND_CROSSOVER) {
                for (int sampleIndex = 0; sampleIndex < SAMPLES_PER_ALGORITHM; sampleIndex++) {
                    String algorithmName = "Evolution Strategy with Crossover and Mutation";
                    String sampleName = String.format("Evolution Strategy with Crossover and Mutation %d", sampleIndex);
                    EvolutionStrategyWithCrossoverAndMutation<TSPSolutionPath, Double> tspESwCM = new EvolutionStrategyWithCrossoverAndMutation<>(
                            (Integer n) -> {
                                Population<TSPSolutionPath> population = new Population<>();
                                for (int i = 0; i < n; i++) {
                                    population.add(initialSolution.mutate());
                                }
                                return population;
                            },
                            (v) -> 0.0 - v.cost(),
                            10,
                            2,
                            (parent1, parent2) -> parent1.crossover(parent2),
                            0.1,
                            (v) -> v.mutate(),
                            (p) -> false
                    );
                    EvolutionResult<TSPSolutionPath, Double> resultESwCM = tspESwCM.run(sampleName, TOTAL_ITERATIONS);
                    Logger.out(String.format("%s - %s", sampleName, resultESwCM.getBestFitness().toString()));
                    if (!resultESwCM.getBestIndividual().validate()) {
                        Logger.out("Invalid solution!");
                        Logger.out(resultESwCM.getBestIndividual().toString());
                    }
                    comparison.addResult(algorithmName, resultESwCM);
                }
            }

            Logger.out(comparison.getSummary());

            if (EXPORT_TO_CSV) {
                comparison.exportToCSV(EXPORT_FILE_PATH);
            }

        } catch (Exception e) {
            Logger.out(e);
        }

    }

}
