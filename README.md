# EOA - Semestrální práce - Trading Evolution

V rámci semestrální práce jsem se snažil využít evoluční algoritmy pro obchodování na burze. V rámci práce jsem se zaměřil na obchodování s kryptoměnami a zejména s měnou Bitcoin z toho důvodu, že je velice volatilní a historie ceny je snadno získatelná v elektronické podobě. Věřím ale, že všechny optimalizační postupy by byly s různou mírou úspěchu použitelné při obchodování na jakékoliv burze.

## Sestavení a spuštění

Projekt používá balíčkovací systém **Maven**, nejjednodušší způsob sestavení je příkaz 

    mvn package
    
Pro vývoj jsem použil prostředí **Intellij IDEA**, jehož projektové soubory jsou připojené.

Main class je **App.java** - zde je program, který spustí porovnání algoritmů. Na začátku metody **main** je několik konstant pomocí kterých lze určit parametry porovnání (který graf řešit, počet iterací, ...).

## Popis zdrojových kódů

V balíčku **eu.zavadil.fel.eoa.core** jsou obecné třídy, které implemetují různé evoluční algoritmy tak, aby bylo možné je snadno použít pro řešení libovolného problému dodáním příslušných funkcí (operátoru křížení, účelové funkce atd...). Tyto třídy jsou vesměs výsledkem práce na cvičních a domácích úkolech.

V balíčku **eu.zavadil.fel.eoa.trading** jsou třídy pro samotné obchodování a vyhodnocování historie - vývoje cen.

## Účelová funkce

Účelová funkce funguje následovně - algoritmus dostane k dispozici pevně danou částku, se kterou může obchodovat a je rovněž určeno, jak často se může dotazovat na vývoj cen a jak často obchoduje (např. jednou za hodinu nebo jednou za den). Algoritmus má k dospozici historii vývoje ceny na burze a z ní dopočítané statistické údaje (střední hodnota, průměrný růst/pokles za jednotku času atd...).

Algoritmus poté projde simulací obchodování - podle zvolené periody je mu předkládána historie vývoje cen na burze a on se musí *rozhodovat* podobně jako skutečný trader. Pokud nemá uzavřenou pozici, může se rozhodnout, že nakoupí obchodovanou komoditu za současnou cenu. Pokud má uzavřenou pozici, může se rozhodnout, že prodá nakoupenou komoditu za současnou cenu.

Pokud má algoritmus uzavřenou pozici v okamžiku ukončení simulace, vše je automaticky prodáno za aktuální cenu.

Rozdíl mezi částkou, se kterou algoritmus začínal, a finální částkou představuje zisk nebo ztrátu z obchodování a použijeme jej jako hodnotu účelové funkce, kterou se (logicky) snažíme maximalizovat. 

Při srovnání algoritmů je nutné, aby měly stejné výchozí podmínky a stejné parametry - výchozí částka, perioda obchodování, historie cen.
 
## Genom

- jednoduchá verze (algoritmus s vektorem)
- gramatická verze

## Mutační operátor



## Operátor křížení


## Algoritmy

### Lokální prohledávání

Třída **LocalSearchFirstImprove** implementuje algoritmus lokálního prohledávání s *hill-climbing* - mutujeme jedince stále dokola a nahradíme jej novým, pokud nalezneme mutaci s lepší fitness.

### Lokální prohledávání s restartem 

Třída **LocalSearchWithRestarts** používá algoritmus lokálního prohledávání s *hill-climbing*, ale namísto jedné kontinuální evoluce jich provede několik - prostě začně znovu od začátku.

### Evoluční strategie s mutací

Stav je reprezentován populací, ale noví jedinci vznikají prostou mutací z převživších jedinců minulé generace.

Oproti lokálnímu prohledávání nepřináší žádné významné výhody - vlastně jen zvýšíme počet mutovaných jedinců. Více jedinců sice znamená menší pravděpodobnost uvíznutí v lokálním maximu, ale fakt, že jedinci existují v jedné populaci zase přináší riziko, že dojde k degradaci genofondu - celá populace se bude skládat z velmi příbuzných jedinců a k uvíznutí stejně dojde.

### Evoluční strategie s křížením

Noví jedinci vznikají zkřížením dvou jedinců z předchozí generace. Rodiče jsou vybírání náhodně.

Algoritmus se vyznačuje rychlejším tempem vývoje než ostatní.
Při absenci mutace ale dochází k degradaci genofondu a v případě tohoto algoritmu je fatální - bez přítomnosti mutace se degradovaná populace již nemá šanci dále vyvíjet.

### Evoluční strategie s mutací i křížením

Noví jedinci vznikají zkřížením dvou náhodně vybraných jedinců z předchozí generace, někteří jsou ale ještě navíc následně zmutováni.

Algoritmus vykazuje srovnatelné tempo růstu jako u ES s křížením, ale degradace populace a uvíznutí v lokálním maximu nemusí být fatální - občasná mutace vnáší do degradované populace nové geny.

